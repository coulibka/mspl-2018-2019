set.seed(42);
times <- rnorm(n = 50, mean = 4, sd = 1.5);

df <- read.csv("pp-synthetic.csv", header=TRUE, col.names=c("time"))
head(df);
{r; fig.width=6; fig.height=3.5}
plot(df$time, ylab="Time (seconds)", xlab="Measurement Number");
summary(df$time);
{r; fig.width=2; fig.height=3.5}
boxplot(df$time, ylab="Time (seconds)")
{r; fig.width=6}
hist(df$time, breaks=10, xlab="Time (seconds)", main="Histogram of Ping-Pong")
sd(df$time)
mean(df$time)
sd(df$time)
{r}
df <- read.csv("data/PP_size_1.csv");
head(df);





