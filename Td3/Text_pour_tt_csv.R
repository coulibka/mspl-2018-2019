
df1 <- read.csv("data/PP_size_1.csv");
df1$size <1

df2 <- read.csv("data/PP_size_2.csv");
df2$size <2

df3<- read.csv("data/PP_size_3.csv");
df3$size <3

df4 <- read.csv("data/PP_size_4.csv");
df4$size <4

df5 <- read.csv("data/PP_size_5.csv");
df5$size <5

df12 <- do.call(rbind, list(df1,df2,df3,df4,df5)) 


{r; fig.width=5; fig.height=3.5}
plot(df$time, ylab="Time (seconds)", xlab="Measurement Number");

{r; fig.width=2; fig.height=3.5}
boxplot(df$time, ylab="Time (seconds)")

{r;fig.width=6}
hist(df$time, breaks=10, xlab="Time (seconds)", main="Histogram of Ping-Pong")