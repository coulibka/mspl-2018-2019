---
title: "TD5BergerVincent"
author: "Axel Vincent & Pierre Berger"
date: "12 février 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown


```{r dpt2016}
dpt2016 <- read.delim("~/MIAGE-DESKTOP-FBEJV6P/S6/Statistiques/MSPL/TD5/dpt2016.txt")
summary(dpt2016)
```

Load the necessary packages:

```{r}
library(dplyr)
library(magrittr)
library(ggplot2)
```

## First name frequency evolves along time

```{r}
dpt2016 %>% filter(preusuel != "_PRENOMS_RARES", annais != "XXXX") %>% group_by(annais, preusuel) %>% summarise(N=n()) %>% arrange(desc(N,annais))
```

## Your Name Here

```{r}
dpt2016 %>% filter(preusuel == 'PIERRE') %>% group_by(dpt, preusuel) %>% summarise(N=n())
dpt2016 %>% filter(preusuel == 'AXEL') %>% group_by(dpt, preusuel) %>% summarise(N=n())
```

## Is there some sort of geographical correlation with the data?


## Which state has a larger variety of names along time?



