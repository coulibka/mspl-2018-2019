set.seed(42);
times <- rnorm(n = 50, mean = 4, sd = 1.5);

df <- read.csv("airmiles.csv", header=TRUE, col.names=c("time"))
head(df);
pressure;

ggplot(data=pressure, aes(x=temperature, y=pressure)) + geom_line();

ggplot(data=pressure, aes(x=temperature, y=pressure)) +
  
  geom_point(mapping = 12, data = df, stat = "identity",
             position = "identity", ..., na.rm = FALSE, show.legend = NA,
             inherit.aes = TRUE)
  
  geom_point() +
  geom_line() +
  xlab("Temperature (deg C)") +
  ylab("Pressure (mm of Hg)")
ggplot(data=mtcars, aes(x=disp, y=hp, size=mpg, color=red)) + 
  geom_point()
str(cars)
ggplot(data=cars) +
  geom_histogram(aes(x=dist));
ggplot(cars, aes(x=dist)) +
  geom_histogram(binwidth = 5)+
  geom_density(aes(y=10 * ..count..));
{r; fig.width=3;fig.height=5}
ggplot(cars, aes(x=1, y=speed)) + geom_boxplot() + geom_point(y=mean(cars$speed))
ggplot(mtcars, aes(x=hp, y=mpg)) + facet_wrap(~gear) + geom_point()

